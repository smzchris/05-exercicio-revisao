package com.itau.cacaniquel;

import java.util.Random;

public class Slot {
	private Simbolo simboloSorteado;
	private Random sorteador = new Random();
	
	public Simbolo sortear() {
		int posicao = sorteador.nextInt(Simbolo.tamanho());
		
		simboloSorteado = Simbolo.values()[posicao];
		
		return simboloSorteado;
	}
	
	public Simbolo getUltimoSorteio() {
		return simboloSorteado;
	}
	
	public String toString() {
		return simboloSorteado.getNome();
	}
}
